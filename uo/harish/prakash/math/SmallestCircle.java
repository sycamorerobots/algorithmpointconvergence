/**
 * 
 */
package uo.harish.prakash.math;

import java.util.ArrayList;
import java.util.Random;
import it.diunipi.volpi.sycamore.engine.Point2D;

/**
 * @author harry
 *
 */
public class SmallestCircle {

	private Point2Dx[]	points;
	private Circle		circle;

	private SmallestCircle(Point2Dx[] points) {

		this.points = points;
		this.circle = null;
	}

	/**
	 * @return the center
	 */
	public Point2D getCenter() {

		if (circle == null) {
			circle = initProcess();
		}
		return circle.getCenter();
	}

	/**
	 * @return the radius
	 */
	public float getRadius() {

		if (circle == null) {
			getCenter();
		}

		return circle.getRadius();
	}

	private Circle initProcess() {

		if ((points == null) || (points.length == 0))
			return null;
		if (points.length == 1)
			return new Circle(points[0], 0);

		Point2Dx v1 = points[0];
		Point2Dx v2 = points[1];

		return computeCenterOfSmallestCircle(v1, v2);
	}

	private Circle computeCenterOfSmallestCircle(Point2Dx v1, Point2Dx v2) {

		ArrayList<Point2Dx> outliers = outliersOfCircleByPoints(v1, v2);
		if (outliers.size() < 1) {
			Point2Dx center = centerOfCircle(v1, v2);
			double radius = center.distanceTo(v1);
			Circle circle = new Circle(center, radius);
			return circle;
		}

		int randomIndex = new Random().nextInt(outliers.size());
		Point2Dx v3 = outliers.get(randomIndex);
		return computeCenterOfSmallestCircle(v1, v2, v3);
	}

	private Circle computeCenterOfSmallestCircle(Point2Dx v1, Point2Dx v2, Point2Dx v3) {

		if (angleAtTheFirstPoint(v1, v2, v3) >= 90)
			return computeCenterOfSmallestCircle(v2, v3);

		if (angleAtTheFirstPoint(v2, v3, v1) >= 90)
			return computeCenterOfSmallestCircle(v1, v3);

		if (angleAtTheFirstPoint(v3, v1, v2) >= 90)
			return computeCenterOfSmallestCircle(v1, v2);

		return step4(v1, v2, v3);
	}

	private Circle step4(Point2Dx v1, Point2Dx v2, Point2Dx v3) {

		Point2Dx center = centerOfCircle(v1, v2, v3);
		ArrayList<Point2Dx> outliers = outliersOfCircleByPoints(v1, v2, v3);
		if (outliers.size() < 1) {
			double radius = center.distanceTo(v1);
			Circle circle = new Circle(center, radius);
			return circle;
		}

		Point2Dx farthestOutlier = null;
		Point2Dx A = null;
		Point2Dx C1 = null;
		Point2Dx C2 = null;
		double distanceFromA = 0;

		for (Point2Dx point : outliers) {

			if (v1.distanceTo(point) > distanceFromA) {
				A = v1;
				C1 = v2;
				C2 = v3;
				farthestOutlier = point;
				distanceFromA = A.distanceTo(point);
			}

			if (v2.distanceTo(point) > distanceFromA) {
				A = v2;
				C1 = v1;
				C2 = v3;
				farthestOutlier = point;
				distanceFromA = A.distanceTo(point);
			}

			if (v3.distanceTo(point) > distanceFromA) {
				A = v3;
				C1 = v1;
				C2 = v2;
				farthestOutlier = point;
				distanceFromA = A.distanceTo(point);
			}
		}

		double directionOfOutlier = dotProductOfOrthogonalVector(A, center, farthestOutlier);
		double directionOfC1 = dotProductOfOrthogonalVector(A, center, C1);

		if ((directionOfOutlier < 0) && (directionOfC1 < 0))
			return computeCenterOfSmallestCircle(A, C2, farthestOutlier);

		if ((directionOfOutlier > 0) && (directionOfC1 > 0))
			return computeCenterOfSmallestCircle(A, C2, farthestOutlier);

		return computeCenterOfSmallestCircle(A, C1, farthestOutlier);
	}

	public static SmallestCircle of(Point2D[] points) {

		ArrayList<Point2Dx> doublePoints = new ArrayList<Point2Dx>();
		for (Point2D point : points)
			doublePoints.add(new Point2Dx(point));

		Point2Dx[] arrayPoints = new Point2Dx[doublePoints.size()];
		arrayPoints = doublePoints.toArray(arrayPoints);
		SmallestCircle smallestCircle = new SmallestCircle(arrayPoints);
		return smallestCircle;
	}

	private ArrayList<Point2Dx> outliersOfCircleByPoints(Point2Dx v1, Point2Dx v2) {

		ArrayList<Point2Dx> outliers = new ArrayList<Point2Dx>();

		Point2Dx center = centerOfCircle(v1, v2);
		double radius = center.distanceTo(v1);

		for (Point2Dx point : points) {

			if (center.distanceTo(point) > (radius + 0.0001)) {
				outliers.add(point);
			}
		}

		return outliers;
	}

	private ArrayList<Point2Dx> outliersOfCircleByPoints(Point2Dx v1, Point2Dx v2, Point2Dx v3) {

		ArrayList<Point2Dx> outliers = new ArrayList<Point2Dx>();

		Point2Dx center = centerOfCircle(v1, v2, v3);
		double radius = center.distanceTo(v1);

		for (Point2Dx point : points) {

			if (center.distanceTo(point) > (radius + 0.0001)) {
				outliers.add(point);
			}
		}

		return outliers;
	}

	private static Point2Dx centerOfCircle(Point2Dx v1, Point2Dx v2) {

		Point2Dx center = new Point2Dx();
		center.x = (v1.x + v2.x) / 2;
		center.y = (v1.y + v2.y) / 2;
		return center;
	}

	private static Point2Dx centerOfCircle(Point2Dx v1, Point2Dx v2, Point2Dx v3) {

		double a = v1.x;
		double b = v1.y;
		double c = v2.x;
		double d = v2.y;
		double e = v3.x;
		double f = v3.y;

		double A = e - c;
		double B = c - a;
		double M = sq(c) + sq(d) - sq(a) - sq(b);
		double N = d - b;
		double O = sq(e) + sq(f) - sq(c) - sq(d);
		double P = f - d;
		double T = 2 * (A * N - B * P);

		double x = (N * O) - (M * P);
		double y = (A * M) - (B * O);

		Point2Dx center = new Point2Dx(x / T, y / T);
		return center;
	}

	private static double dotProductOfOrthogonalVector(Point2Dx p1, Point2Dx p2, Point2Dx unknown) {

		double a = p1.x;
		double b = p1.y;
		double c = p2.x;
		double d = p2.y;
		double x = unknown.x;
		double y = unknown.y;

		double productOfOrthAndVector = d * (-x + c + a) + b * (x - c) + c * (y - d) - a * y;
		return productOfOrthAndVector;
	}

	private static double angleAtTheFirstPoint(Point2Dx v1, Point2Dx v2, Point2Dx v3) {

		double a = v1.distanceTo(v2);
		double b = v1.distanceTo(v3);
		double c = v2.distanceTo(v3);

		double cosvalue = (sq(a) + sq(b) - sq(c)) / (2 * a * b);
		double angleRad = Math.acos(cosvalue);
		double angleDeg = Math.toDegrees(angleRad);

		return angleDeg;
	}

	private static double sq(double value) {

		return value * value;
	}
}

class Circle {

	private Point2Dx	center;
	private double		radius;

	public float getRadius() {

		return (float) radius;
	}

	public void setRadius(double radius) {

		this.radius = radius;
	}

	public Point2D getCenter() {

		return center.getSycamorePoint();
	}

	public void setCenter(Point2Dx center) {

		this.center = center;
	}

	public Circle(Point2Dx center, double radius) {
		this.radius = radius;
		this.center = center;
	}
}
