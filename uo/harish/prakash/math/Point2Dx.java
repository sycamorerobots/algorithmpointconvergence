package uo.harish.prakash.math;

import it.diunipi.volpi.sycamore.engine.Point2D;

public class Point2Dx {

	public double	x;
	public double	y;

	public Point2Dx() {

		this.x = 0;
		this.y = 0;
	}

	public Point2Dx(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public Point2Dx(Point2D point) {

		if (point == null) {
			this.x = 0;
			this.y = 0;
		}
		else {
			this.x = point.x;
			this.y = point.y;
		}
	}

	public Point2D getSycamorePoint() {

		return new Point2D((float) x, (float) y);
	}

	public double distanceTo(Point2Dx point) {

		double diffInX = point.x - x;
		double diffInY = point.y - y;

		diffInX = diffInX * diffInX;
		diffInY = diffInY * diffInY;

		double distance = Math.sqrt(diffInX + diffInY);
		return distance;
	}

	public double distanceTo(Point2D point) {

		Point2Dx Point = new Point2Dx(point);
		return distanceTo(Point);
	}
}
