/**
 * 
 */
package uo.harish.prakash.demo;

import uo.harish.prakash.math.SmallestCircle;
import it.diunipi.volpi.sycamore.engine.Point2D;

/**
 * @author harry
 *
 */
public class ShortestCircleTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Point2D p1 = new Point2D(-2, 3);
		Point2D p2 = new Point2D(1, 5);
		Point2D p3 = new Point2D(5, 5);
		Point2D p4 = new Point2D(2, 3);
		Point2D p5 = new Point2D(5, 2);
		Point2D p6 = new Point2D(1, 1);
		Point2D p7 = new Point2D(-2, -1);
		Point2D p8 = new Point2D(3, -1);

		Point2D[] points = new Point2D[] { p1, p2, p3, p4, p5, p6, p7, p8 };
		SmallestCircle smc = SmallestCircle.of(points);
		System.out.println("Center = " + smc.getCenter());
		System.out.println("Radius = " + smc.getRadius());

	}

}
