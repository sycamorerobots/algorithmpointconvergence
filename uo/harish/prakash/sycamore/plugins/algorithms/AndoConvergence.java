/**
 *
 */
package uo.harish.prakash.sycamore.plugins.algorithms;

import java.util.ArrayList;
import java.util.Vector;
import net.xeoh.plugins.base.annotations.PluginImplementation;
import uo.harish.prakash.math.SmallestCircle;
import it.diunipi.volpi.sycamore.engine.Observation;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreObservedRobot;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.plugins.algorithms.AlgorithmImpl;
import it.diunipi.volpi.sycamore.plugins.visibilities.VisibilityImpl;

/**
 * @author harry
 *
 */
@PluginImplementation
public class AndoConvergence extends AlgorithmImpl<Point2D> {

	private static final String	_author				= "Harish Prakash";
	private static final TYPE	_type				= TYPE.TYPE_2D;
	private static final String	_references			= null;
	private static final String	_shortDescription	= "Point Convergence";
	private static final String	_description		= "Distributed Memoryless Point Convergence Algorithm for Mobile Robots With Limited Visibility";
	private static final String	_pluginName			= "PointConvergence";
	private Long				_terminateAt;

	@Override
	public void init(SycamoreObservedRobot<Point2D> robot) {

		_terminateAt = null;
	}

	@Override
	public Point2D compute(Vector<Observation<Point2D>> observations, SycamoreObservedRobot<Point2D> caller) {

		float LIMIT;
		float GOAL;
		Point2D destination;

		if (_terminateAt == null) {
			_terminateAt = System.currentTimeMillis() + (long) (((double) 15000) / caller.getSpeed());
		}

		else if (System.currentTimeMillis() > _terminateAt) {
			setFinished(true);
			_terminateAt = null;
			return null;
		}

		SmallestCircle smallestCircle = SmallestCircle.of(getAllPoints(observations, caller));
		GOAL = caller.getLocalPosition().distanceTo(smallestCircle.getCenter());
		LIMIT = findLimit(caller, smallestCircle, getAllPoints(observations));

		if (LIMIT < GOAL) {
			destination = destinationTowardsCenter(LIMIT, GOAL, caller.getLocalPosition(), smallestCircle.getCenter());
		}
		else {
			destination = smallestCircle.getCenter();
		}

		setFinished(isFinished(observations, caller));
		caller.setDirection(destination);
		return destination;
	}

	private boolean isFinished(Vector<Observation<Point2D>> observations, SycamoreObservedRobot<Point2D> caller) {

		Point2D previous = caller.getLocalPosition();
		for (Observation<Point2D> observation : observations) {
			if (previous.differsModuloEpsilon(observation.getRobotPosition())) {
				return false;
			}
		}

		return true;
	}

	private Point2D destinationTowardsCenter(float distance, float GOAL, Point2D me, Point2D center) {

		double cos = (center.x - me.x) / GOAL;
		double sin = (center.y - me.y) / GOAL;

		double diffX = distance * cos;
		double diffY = distance * sin;

		Point2D destination = new Point2D();
		destination.x = me.x + (float) diffX;
		destination.y = me.y + (float) diffY;

		return destination;
	}

	private float findLimit(SycamoreObservedRobot<Point2D> caller, SmallestCircle circle, Point2D[] points) {

		float lowestLimit = Float.MAX_VALUE;
		Point2D Ri = caller.getLocalPosition();
		Point2D center = circle.getCenter();

		for (Point2D Rj : points) {

			float limitToPoint = findLimitToPoint(Ri, Rj, center);
			if (limitToPoint < lowestLimit) {
				lowestLimit = limitToPoint;
			}
		}

		return lowestLimit;
	}

	private float findLimitToPoint(Point2D Ri, Point2D Rj, Point2D center) {

		double Dj = Ri.distanceTo(Rj);
		double V = VisibilityImpl.getVisibilityRange();
		double ThetaJ = Math.toRadians(angleAtTheFirstPoint(Ri, center, Rj));

		double Part1 = ((Dj / 2)) * Math.cos(ThetaJ);
		double Part3 = sqr(V / 2) - sqr((Dj / 2) * Math.sin(ThetaJ));
		double Part2 = Math.sqrt(Part3);

		double limit = Part1 + Part2;
		float Limit = (float) limit;
		return Limit;
	}

	@Override
	public String getReferences() {

		return _references;
	}

	@Override
	public TYPE getType() {

		return _type;
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginShortDescription() {

		return _shortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _description;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return null;
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}

	private Point2D[] getAllPoints(Vector<Observation<Point2D>> observations, SycamoreObservedRobot<Point2D> caller) {

		Point2D callerPosition = caller.getLocalPosition();
		ArrayList<Point2D> observedPositions = new ArrayList<Point2D>();

		for (Observation<Point2D> observed : observations) {
			observedPositions.add(observed.getRobotPosition());
		}

		observedPositions.add(callerPosition);
		removeDuplicates(observedPositions);

		Point2D[] points = new Point2D[observedPositions.size()];
		points = observedPositions.toArray(points);
		return points;
	}

	private Point2D[] getAllPoints(Vector<Observation<Point2D>> observations) {

		ArrayList<Point2D> observedPositions = new ArrayList<Point2D>();

		for (Observation<Point2D> observed : observations) {
			observedPositions.add(observed.getRobotPosition());
		}

		removeDuplicates(observedPositions);

		Point2D[] points = new Point2D[observedPositions.size()];
		points = observedPositions.toArray(points);
		return points;
	}

	private void removeDuplicates(ArrayList<Point2D> points) {

		if (points == null) {
			return;
		}
		for (int x = 0; x < points.size(); x++) {
			for (int y = x + 1; y < points.size(); y++) {
				if (points.get(x).distanceTo(points.get(y)) == 0) {
					points.remove(y--);
				}
			}
		}
	}

	private static float angleAtTheFirstPoint(Point2D v1, Point2D v2, Point2D v3) {

		float a = v1.distanceTo(v2);
		float b = v1.distanceTo(v3);
		float c = v2.distanceTo(v3);

		float cosvalue = (sq(a) + sq(b) - sq(c)) / (2 * a * b);
		double angleRad = Math.acos(cosvalue);
		double angleDeg = Math.toDegrees(angleRad);

		return (float) angleDeg;
	}

	private static float sq(float value) {

		return value * value;
	}

	public static double sqr(double value) {

		return value * value;
	}
}
